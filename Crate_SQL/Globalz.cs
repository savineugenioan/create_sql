﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Crate_SQL
{
    public class Globalz
    {
        public static void LoadingGif(object y)
        {
            try
            {
                Control x = (Control)y;
                Form f = x.FindForm();
                PictureBox gif = new PictureBox();
                gif.Image = Properties.Resources.loader_gif_transparent_background_1;
                gif.Location = new Point(x.Location.X - 40, x.Location.Y);
                gif.SizeMode = PictureBoxSizeMode.StretchImage;
                gif.Size = new Size(x.Height, x.Height);
                f.Controls.Add(gif);
            }
            catch { };
        }
        public static void del_gif(Form f)
        {
            foreach (PictureBox item in f.Controls.OfType<PictureBox>())
                if (item.Name == "")
                {
                    f.Controls.Remove(item);
                }
        }
    }
}
