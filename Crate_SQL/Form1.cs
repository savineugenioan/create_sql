﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Crate_SQL
{
    public partial class Form1 : Form
    {
        string desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        int[] exceptii_ghilimele;
        string DatabaseStringChar = @"'";
        public Form1()
        {
            InitializeComponent();
            backgroundWorker1.DoWork += new DoWorkEventHandler(main);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                fbd.Filter = "Excel files (*.xls or *.xlsx)|*.xls;*.xlsx";
                fbd.InitialDirectory = desktop_path;
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    textBox1.Text = fbd.FileName;
                }
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            progressBar1.Maximum = 100;
            if (verificari_initiale())
            {
                setDatabaseStringChar();
                progressBar1.Visible = true;
                if (!backgroundWorker1.IsBusy)
                {
                    Globalz.LoadingGif(this.button1);
                    backgroundWorker1.RunWorkerAsync();
                    //progressBar1.Visible = false;
                }
            }

        }

        private void setDatabaseStringChar()
        {
            if (this.SQLServer.Checked)
            {
                DatabaseStringChar= @"'";
            }
            else if(this.MySQL.Checked)
            {
                DatabaseStringChar = "\"";
            }
        }

        private void main(object sender, DoWorkEventArgs e)
        {
            try
            {
                string path = desktop_path + "\\SQL.txt";
                string denumire_tabela = textBox3.Text.Trim().ToUpper();

                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                ExcelApi xl = new ExcelApi(textBox1.Text);
                string[] table_structure = xl.GetRowData(1);
                File.AppendAllText(path, $"INSERT INTO {denumire_tabela} ( ");
                for (int i = 0; i < table_structure.Length - 1; i++)
                {
                    File.AppendAllText(path, table_structure[i] + " , ");
                    backgroundWorker1.ReportProgress( 100 / xl.RowsCount);
                }
                File.AppendAllText(path, table_structure[table_structure.Length - 1] + " )" + Environment.NewLine + "VALUES" + Environment.NewLine);
                string[] row_data;
                for (int i = 2; i < xl.RowsCount ; i++)
                {
                    File.AppendAllText(path, "( ");
                    row_data = xl.GetRowData(i);
                    for (int w = 0; w < row_data.Length; w++)
                    {
                        if (row_data[w]== null || row_data[w]=="")
                            row_data[w] = "NULL";
                    }


                    for (int j = 0; j < row_data.Length - 1; j++)
                    {
                        if (exceptii_ghilimele.Contains(j) || row_data[j]=="NULL")
                        {
                            File.AppendAllText(path, row_data[j] + " , ");
                        }
                        else
                        {
                            File.AppendAllText(path, DatabaseStringChar + row_data[j] + DatabaseStringChar + " , ");
                        }
                    }
                    if (exceptii_ghilimele.Contains(row_data.Length - 1) || row_data[row_data.Length - 1] == "NULL")
                    {
                        File.AppendAllText(path, row_data[row_data.Length - 1]);
                    }
                    else
                    {
                        File.AppendAllText(path, DatabaseStringChar + row_data[row_data.Length - 1] + DatabaseStringChar);
                    }
                    File.AppendAllText(path, " )," + Environment.NewLine);


                    backgroundWorker1.ReportProgress(i*100/ xl.RowsCount);
                }
                File.AppendAllText(path, "( ");

                row_data = xl.GetRowData(xl.RowsCount);


                for (int j = 0; j < row_data.Length - 1; j++)
                {
                    if (exceptii_ghilimele.Contains(j) || row_data[j] == "NULL")
                    {
                        File.AppendAllText(path, row_data[j] + " , ");
                    }
                    else
                    {
                        File.AppendAllText(path, DatabaseStringChar + row_data[j] + DatabaseStringChar + " , ");
                    }
                }
                if (exceptii_ghilimele.Contains(row_data.Length - 1) || row_data[row_data.Length - 1] == "NULL")
                {
                    File.AppendAllText(path, row_data[row_data.Length - 1]);
                }
                else
                {
                    File.AppendAllText(path, DatabaseStringChar + row_data[row_data.Length - 1] + DatabaseStringChar);
                }
                File.AppendAllText(path, " );" + Environment.NewLine);


                backgroundWorker1.ReportProgress((xl.RowsCount) * 100 / xl.RowsCount);

                xl.CloseExcel();
                MessageBox.Show($"Fisierul {path} a fost creat cu succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                backgroundWorker1.ReportProgress(0);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private bool verificari_initiale()
        {
            char[] separators = { ' ', ',' };
            exceptii_ghilimele = Array.Empty<int>();

            if (textBox1.Text.Trim() == "" || textBox1.Text.Trim() == null)
            {
                MessageBox.Show("Alegeti un fisier XL!", "Fisier XL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (textBox3.Text.Trim() == "" || textBox3.Text.Trim() == null)
            {
                MessageBox.Show("Introduceti denumirea tabelei", "Eroare Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                exceptii_ghilimele = textBox2.Text.Split(separators, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
                return true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Eroare in coloana de exceptii", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage <= progressBar1.Maximum)
                progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Globalz.del_gif(this);
            progressBar1.Value = 0;
            progressBar1.Visible= false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.SQLServer.Select();
        }
    }
}
