﻿using System;
using System.Runtime.InteropServices;
using xl = Microsoft.Office.Interop.Excel;

namespace Crate_SQL
{
    class ExcelApi
    {
        xl.Application xlApp = null;
        xl.Workbooks workbooks = null;
        xl.Workbook workbook = null;
        xl.Worksheet worksheet;
        public string xlFilePath;
        public int ColumnsCount;
        public int RowsCount;

        public ExcelApi(string xlFilePath)
        {
            this.xlFilePath = xlFilePath;
            xlApp = new xl.Application();
            workbooks = xlApp.Workbooks;
            workbook = workbooks.Open(xlFilePath);
            worksheet = workbook.Worksheets[1] as xl.Worksheet;
            xl.Range range = worksheet.UsedRange;
            ColumnsCount = range.Columns.Count;
            RowsCount = range.Rows.Count;
        }



        public void CloseExcel()
        {
            Marshal.FinalReleaseComObject(worksheet);
            workbook.Close(false, xlFilePath, null); // Close the connection to workbook
            Marshal.FinalReleaseComObject(workbook); // Release unmanaged object references.
            workbook = null;

            workbooks.Close();
            Marshal.FinalReleaseComObject(workbooks);
            workbooks = null;

            xlApp.Quit();
            Marshal.FinalReleaseComObject(xlApp);
            xlApp = null;
        }

        public string[] GetColumnData(int colNumber)
        {
            xl.Range range = worksheet.UsedRange;
            string[] values = new string[RowsCount];

            for (int i = 1; i <= RowsCount; i++)
            {
                values[i - 1] = Convert.ToString((range.Cells[i, colNumber] as xl.Range).Value2).Trim();
            }

            return values;
        }
        public string[] GetRowData(int rowNumber)
        {
            xl.Range range = worksheet.UsedRange;
            string[] values = new string[ColumnsCount];

            for (int i = 1; i <= ColumnsCount; i++)
            {
                values[i - 1] = Convert.ToString((range.Cells[rowNumber, i] as xl.Range).Value2);
            }

            return values;
        }
    }
}